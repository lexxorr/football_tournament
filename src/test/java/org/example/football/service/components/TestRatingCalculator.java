package org.example.football.service.components;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;
import java.util.TreeMap;


@ExtendWith(SpringExtension.class)
public class TestRatingCalculator{

    @SpyBean
    private RatingCalculator ratingCalculator;
    @Test
    public void testCalcOneToOneRating () throws Exception{
        TreeMap<Long, Integer> r1 = new TreeMap<>( Map.of(1L, 1790, 3L, 1770));
        TreeMap<Long, Integer> r2 = new TreeMap<> ( Map.of(2L, 1800, 4L, 1800));
        ratingCalculator.calcRating(r1, r2, true);
        System.out.println(r1);
        System.out.println(r2);
        assertNotEquals(r1, r2);
    }
}