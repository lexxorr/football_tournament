ALTER TABLE IF EXISTS users ADD COLUMN confirm_token character varying(255) NOT NULL default 'token';
ALTER TABLE IF EXISTS users ADD COLUMN is_enabled boolean NOT NULL default 'false';

insert into role (name) values ('ROLE_USER_CONFIRMED');