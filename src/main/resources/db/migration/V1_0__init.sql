CREATE TABLE IF NOT EXISTS users
(
    id BIGSERIAL PRIMARY KEY,
    login character varying(255) UNIQUE NOT NULL,
    password character varying(255) NULL
);

CREATE TABLE IF NOT EXISTS role
(
    id BIGSERIAL PRIMARY KEY,
    name character varying(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS user_role
(
    id BIGSERIAL PRIMARY KEY,
    user_id bigint,
    role_id bigint,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (role_id) REFERENCES role(id)
);

CREATE TABLE IF NOT EXISTS players
(
    id BIGSERIAL PRIMARY KEY,
    user_id bigint,
    nick character varying(255) UNIQUE NOT NULL,
    name character varying(255) NOT NULL,
    surname character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    slack_uid character varying(255) NOT NULL,
    mattermost_channel_id character varying(255),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS match_states(
    id BIGSERIAL PRIMARY KEY,
    state character varying(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS match_history (
    id BIGSERIAL PRIMARY KEY,
    begin_time timestamp,
    team_one_ids bigint[],
    team_two_ids bigint[],
    team_one_score integer DEFAULT 0,
    team_two_score integer DEFAULT 0,
    state_id bigint DEFAULT 1,
    FOREIGN KEY (state_id) REFERENCES match_states(id)
);

CREATE TABLE IF NOT EXISTS personal_leaderboard (
    id BIGSERIAL PRIMARY KEY,
    player_id bigint,
    score integer DEFAULT 0,
    wins integer DEFAULT 0,
    loses integer DEFAULT 0,
    goals_scored integer DEFAULT 0,
    goals_passed integer DEFAULT 0,
    FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS mixed_leaderboard (
    id BIGSERIAL PRIMARY KEY,
    player_id bigint,
    score integer DEFAULT 0,
    wins integer DEFAULT 0,
    loses integer DEFAULT 0,
    goals_scored integer DEFAULT 0,
    goals_passed integer DEFAULT 0,
    FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS team_leaderboard (
    id BIGSERIAL PRIMARY KEY,
    player_id bigint,
    score integer DEFAULT 0,
    wins integer DEFAULT 0,
    loses integer DEFAULT 0,
    goals_scored integer DEFAULT 0,
    goals_passed integer DEFAULT 0,
    FOREIGN KEY (player_id) REFERENCES players(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS invite_states(
    id BIGSERIAL PRIMARY KEY,
    state character varying(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS request_queue(
     id BIGSERIAL PRIMARY KEY,
     begin_time timestamp,
     uuid character varying(255) UNIQUE NOT NULL,
     inviters bigint[],
     acceptors bigint[]
);

insert into users (login, password)values ('admin', '$2a$12$UNixpFUSpJ3huXHrkznYo.4ypaAgaRPJIMLDrnOeqGc6rdi4mfXWW');

insert into role (name) values ('ROLE_ADMIN');
insert into role (name) values ('ROLE_USER');

insert into user_role (user_id, role_id) values (1, 1);

insert into match_states (state) values ('PLANED');
insert into match_states (state) values ('STARTED');
insert into match_states (state) values ('PAUSED');
insert into match_states (state) values ('DELAYED');
insert into match_states (state) values ('FINISHED');
insert into match_states (state) values ('CANCELED');
insert into match_states (state) values ('ABSENCE');

insert into invite_states (state) values ('waiting for confirmation');
insert into invite_states (state) values ('accepted');
insert into invite_states (state) values ('rejected');