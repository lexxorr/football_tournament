package org.example.football.api;

import org.example.football.enums.ApiMessageTypes;
import org.example.football.exception.ApiCallArgumentsMissedException;
import org.example.football.payload.request.mattermostapi.AbstractMattermostRequest;
import org.example.football.payload.request.mattermostapi.MattermostInviteRequest;
import org.example.football.payload.request.mattermostapi.MattermostMatchStartNotificationRequest;
import org.example.football.payload.response.mattermostapi.MattermostUserIdByEmailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class MatterMostApiHandler extends ApiHandler{

    @Value("${mattermost.bot.id}")
    private String botId;

    @Override
    public <T extends AbstractMattermostRequest> void sendMessage(ApiMessageTypes messageType, HashMap<String, String> apiArguments, T messageBody)
            throws ApiCallArgumentsMissedException {
        System.out.println(apiArguments);
        try {
            switch (messageType){
                case INVITE_PLAYER_MESSAGE: sendInviteToMatch(apiArguments, messageBody);
                    break;
                case NOTIFY_MATCH_START:
                    sendNotificationToUser(messageBody);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + messageType);
            }
        } catch (ApiCallArgumentsMissedException e){
            throw e;
        }
    }

    @Autowired
    public MatterMostApiHandler(@Value("${mattermost.bot.content}") String contentType,
                                @Value("${mattermost.bot.token}") String authToken) {
        super(contentType, authToken);
    }

    public String sendFirstInviteToMatch(String opponentMail){
        String urlUserId = String.format("https://madbrains.cloud.mattermost.com/api/v4/users/email/%s", opponentMail);
        ResponseEntity<MattermostUserIdByEmailResponse> idResponse = call(urlUserId, HttpMethod.GET, null, MattermostUserIdByEmailResponse.class);
        String opponentId = idResponse.getBody().getId();

        String urlUserChannelId = "https://madbrains.cloud.mattermost.com/api/v4/channels/direct";
        ResponseEntity<MattermostUserIdByEmailResponse> idChannelResponse = call(urlUserChannelId, HttpMethod.POST,
                String.format("[\"%s\", \"%s\"]", botId, opponentId), MattermostUserIdByEmailResponse.class);
        String opponentChannelId = idChannelResponse.getBody().getId();

        return opponentChannelId;
    }

    public <T> void sendInviteToMatch(HashMap<String, String> apiArguments, T body) throws ApiCallArgumentsMissedException {
        if (!apiArguments.containsKey("userId"))
            throw new ApiCallArgumentsMissedException("userId");
        if (! (body instanceof MattermostInviteRequest))
            throw new IllegalArgumentException( body.getClass().toString() + " incompatible with type " + ApiMessageTypes.INVITE_PLAYER_MESSAGE);
        if (apiArguments.get("userId").isEmpty()) {
            if (!apiArguments.containsKey("email") || apiArguments.get("email").isEmpty())
                throw new ApiCallArgumentsMissedException("email");
            String opponentChanelId = sendFirstInviteToMatch(apiArguments.get("email"));
            apiArguments.put("userId", opponentChanelId);
        }
        MattermostInviteRequest mattermostInviteRequest = (MattermostInviteRequest) body;
        mattermostInviteRequest.setOpponentChannelId(apiArguments.get("userId"));
        mattermostInviteRequest.generate();
        System.out.println(body);

        String urlUserMessage = "https://madbrains.cloud.mattermost.com/api/v4/posts";
        call(urlUserMessage, HttpMethod.POST, mattermostInviteRequest, void.class);
    }

    public <T> void sendNotificationToUser(T body){
        if(! (body instanceof MattermostMatchStartNotificationRequest))
            throw new IllegalArgumentException( body.getClass().toString() + " incompatible with type " + ApiMessageTypes.NOTIFY_MATCH_START);
        String urlUserMessage = "https://madbrains.cloud.mattermost.com/api/v4/posts";
        call(urlUserMessage, HttpMethod.POST, body, void.class);
    }
}
