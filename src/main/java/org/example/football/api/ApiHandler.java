package org.example.football.api;

import org.example.football.enums.ApiMessageTypes;
import org.example.football.exception.ApiCallArgumentsMissedException;
import org.example.football.mapper.AbstractDto;
import org.example.football.mapper.AbstractEntity;
import org.example.football.mapper.AbstractMapper;
import org.example.football.payload.request.mattermostapi.AbstractMattermostRequest;
import org.example.football.service.components.DataModification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;


public abstract class ApiHandler {
    private final RestTemplate restTemplate;

    @Value("${app.outer}")
    private String appServer;

    @Value("${server.port}")
    private String appPort;

    private HttpHeaders httpHeaders;

    protected AbstractMapper<AbstractEntity, AbstractDto> mapper = null;

    public abstract <T extends AbstractMattermostRequest> void sendMessage(ApiMessageTypes messageType, HashMap<String, String> apiArguments, T messageBody)
            throws ApiCallArgumentsMissedException, IllegalStateException;

//    public <T  extends AbstractMattermostRequest> void sendMessage(ApiMessageTypes messageType, HashMap<String, String> apiArguments, T messageBody,
//                                DataModification dataModification) throws ApiCallArgumentsMissedException, IllegalStateException {
//        sendMessage(messageType, apiArguments, messageBody);
//        dataModification.go("");
//    }

    public ApiHandler(String contentType, String authToken) {
        restTemplate = new RestTemplateBuilder().build();
        createHttpHeader(contentType, authToken);
    }

    protected  <T, R> ResponseEntity<R> call(String url, HttpMethod httpMethod,  T body, Class<R> classToConvert) {
        HttpEntity requestEntity = new HttpEntity(body, httpHeaders);
        try {
            System.out.println(requestEntity);
            return restTemplate.exchange(url, httpMethod, requestEntity, classToConvert);
        }
        catch (RestClientException e){
            throw e;
        }
    }

    public String getAppUrl(){
        return appServer;
    }

    private void createHttpHeader(String contentType, String authToken){
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "Bearer " + authToken);
        httpHeaders.set("Content-type", contentType);
    }

}
