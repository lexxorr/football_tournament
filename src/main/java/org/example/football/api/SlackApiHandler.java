package org.example.football.api;

import org.example.football.enums.ApiMessageTypes;
import org.example.football.exception.ApiCallArgumentsMissedException;
import org.example.football.payload.request.mattermostapi.AbstractMattermostRequest;
import org.example.football.payload.response.appapi.SlackRequestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class SlackApiHandler extends ApiHandler{
    @Override
    public <T extends AbstractMattermostRequest> void sendMessage(ApiMessageTypes messageType, HashMap<String, String> apiArguments, T messageBody)
            throws ApiCallArgumentsMissedException {

    }

    @Autowired
    public SlackApiHandler(@Value("${slack.app.content}") String contentType,
                           @Value("${slack.app.token}") String authToken ) {
        super(contentType, authToken);
    }

    public void sendMessageToUser(String slackUid, String requestId, String date, String callerName) {
        String url = "https://slack.com/api/chat.postMessage";
        SlackRequestResponse request = SlackRequestResponse.generate(callerName,
                slackUid,
                date,
                requestId);
        call(url, HttpMethod.POST, request,  void.class);
    }

}