package org.example.football.entity;

import lombok.Data;
import lombok.ToString;
import org.example.football.model.User;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import javax.transaction.Transactional;

@Data
@Entity
@Table(name = "players")
@Proxy(lazy=false)
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nick;

    @Column(name = "user_id")
    private Long userId;

    private String name;

    private String surname;

    private String email;

    @Column(name = "slack_uid")
    private String slackUID;

    @Column(name = "mattermost_channel_id")
    private String matterMostChannelId;

    @OneToOne(mappedBy = "players", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @ToString.Exclude
    private User user;

    @OneToOne
    @ToString.Exclude
    @JoinTable(
            name = "personal_leaderboard",
            joinColumns = @JoinColumn(name = "player_id", referencedColumnName = "id")
    )
    private PersonalLeaderboard personal_leaderboard;
}
