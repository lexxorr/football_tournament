package org.example.football.entity;

public enum InviteStateEnum {
    INVITE_UNKNOWN,
    INVITE_DRAFT,
    INVITE_ACCEPTED,
    INVITE_REJECTED
}
