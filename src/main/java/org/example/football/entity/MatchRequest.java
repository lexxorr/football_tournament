package org.example.football.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "request_queue")
public class MatchRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ElementCollection
    private List<Long> inviters;

    @ElementCollection
    private List<Long> acceptors;

    private String uuid;

    @Column(name = "begin_time")
    private Date beginTime;

    @Column(name = "accepted_count")
    private Integer acceptedCount;

}
