package org.example.football.entity.MatchRequsetEntities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;

@Data
public class Block {
    public void addField(Field field){
        fields.add(field);
    }

    public void addElement(Element element) {
        elements.add(element);
    }
    @JsonProperty("type")
    private String type;

    @JsonProperty("text")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Text text;

    @JsonProperty("fields")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<Field> fields = new ArrayList<>();

    @JsonProperty("elements")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ArrayList<Element> elements = new ArrayList<>();
}
