package org.example.football.entity.MatchRequsetEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Element {
    @JsonProperty("type")
    private String type;

    @JsonProperty("text")
    private Text text;

    @JsonProperty("style")
    private String style;

    @JsonProperty("value")
    private String value;
}
