package org.example.football.entity.MatchRequsetEntities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Text {
    public Text(String text, String type, Boolean emoji){

        this.text = text;
        this.type = type;
        this.emoji = emoji;
    }

    public Text (String text){

        this.text = text;
        this.type = null;
        this.emoji = null;
    }

    @JsonProperty("type")
    private String type;

    @JsonProperty("text")
    private String text;

    @JsonProperty("emoji")
    private Boolean emoji;
}
