package org.example.football.entity.MatchRequsetEntities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Field {

    @JsonProperty("type")
    private String type;

    @JsonProperty("text")
    private String text;
}
