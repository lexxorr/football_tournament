package org.example.football.entity;

import lombok.Data;
import org.example.football.enums.MatchStateEnum;
import org.example.football.enums.converters.MatchStateEnumConverter;
import org.example.football.mapper.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "match_history")
public class Match extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "begin_time")
    private Date beginTime;

    @ElementCollection
    @Column(name = "team_one_ids")
    private List<Long> teamOne;

    @ElementCollection
    @Column(name = "team_two_ids")
    private List<Long> teamTwo;

    @Column(name = "team_one_score")
    private Integer teamOneScore = 0;

    @Column(name = "team_two_score")
    private Integer teamTwoScore = 0;

    @Column(name = "state_id")
    @Convert(converter = MatchStateEnumConverter.class)
    private MatchStateEnum state;
}
