package org.example.football.entity.mattermostmessage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ActionContext {

    @JsonProperty("action")
    private String action;
}
