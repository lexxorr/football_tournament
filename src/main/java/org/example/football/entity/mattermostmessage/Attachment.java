package org.example.football.entity.mattermostmessage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Attachment {

    @JsonProperty("pretext")
    private String pretext;

    @JsonProperty("text")
    private String text;

    @JsonProperty("actions")
    private Action[] actions;
}
