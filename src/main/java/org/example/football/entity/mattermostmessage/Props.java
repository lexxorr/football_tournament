package org.example.football.entity.mattermostmessage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Props {

    @JsonProperty("attachments")
    private Attachment[] attachments;

}
