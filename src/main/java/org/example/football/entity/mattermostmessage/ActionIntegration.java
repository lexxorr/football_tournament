package org.example.football.entity.mattermostmessage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ActionIntegration {

    @JsonProperty("url")
    private String url;

    @JsonProperty("context")
    private ActionContext context;
}
