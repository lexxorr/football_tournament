package org.example.football.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table(name = "personal_leaderboard")
public class PersonalLeaderboard
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer score = 0;

    private Integer wins = 0;

    private Integer loses = 0;

    @Column(name = "goals_scored")
    private Integer goalsScored = 0;

    @Column(name = "goals_passed")
    private Integer goalsPassed = 0;

    @OneToOne(mappedBy = "personal_leaderboard", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private Player player;
}
