package org.example.football.mapper.impl;

import org.example.football.entity.Match;
import org.example.football.mapper.AbstractMapper;
import org.example.football.payload.request.appapi.MatchResultRequest;
import org.springframework.stereotype.Component;

@Component
public class MatchMapper extends AbstractMapper<Match, MatchResultRequest> {
    public MatchMapper(){
        super(Match.class, MatchResultRequest.class);
    }
}
