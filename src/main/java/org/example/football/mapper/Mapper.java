package org.example.football.mapper;

public interface Mapper <E extends AbstractEntity, D extends AbstractDto>{
    D toDto(E entity);
    E toEntity(D dto);
}
