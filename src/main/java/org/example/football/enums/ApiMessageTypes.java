package org.example.football.enums;

public enum ApiMessageTypes {
    INVITE_PLAYER_MESSAGE,
    NOTIFY_MATCH_START
}
