package org.example.football.enums.converters;

import org.example.football.enums.MatchStateEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MatchStateEnumConverter implements AttributeConverter<MatchStateEnum, Integer> {

    @Override
    public Integer convertToDatabaseColumn(MatchStateEnum matchStateEnum) {
        if (matchStateEnum == null)
            return null;
        switch (matchStateEnum){
            case PLANED:
                return 1;
            case STARTED:
                return 2;
            case PAUSED:
                return 3;
            case DELAYED:
                return 4;
            case FINISHED:
                return 5;
            case CANCELED:
                return 6;
            case ABSENCE:
                return 7;
            default:
                throw new IllegalArgumentException(matchStateEnum + " not supported");
        }
    }

    @Override
    public MatchStateEnum convertToEntityAttribute(Integer dbData) {
        if (dbData == null)
            return null;
        switch (dbData){
            case 1:
                return MatchStateEnum.PLANED;
            case 2:
                return MatchStateEnum.STARTED;
            case 3:
                return MatchStateEnum.PAUSED;
            case 4:
                return MatchStateEnum.DELAYED;
            case 5:
                return MatchStateEnum.FINISHED;
            case 6:
                return MatchStateEnum.CANCELED;
            case 7:
                return MatchStateEnum.ABSENCE;
            default:
                throw new IllegalArgumentException(dbData + " not supported");

        }
    }
}