package org.example.football.enums.converters;

import org.example.football.enums.RolesEnum;

import javax.persistence.AttributeConverter;

public class RolesEnumConverter implements AttributeConverter<RolesEnum, Integer> {
    @Override
    public Integer convertToDatabaseColumn(RolesEnum rolesEnum) {
        switch (rolesEnum) {
            case ROLE_ADMIN:
                return 1;
            case ROLE_USER:
                return 2;
            case ROLE_USER_CONFIRMED:
                return 3;
        }
        return null;
    }

    @Override
    public RolesEnum convertToEntityAttribute(Integer integer) {
        switch (integer) {
            case 1:
                return RolesEnum.ROLE_ADMIN;
            case 2:
                return RolesEnum.ROLE_USER;
            case 3:
                return RolesEnum.ROLE_USER_CONFIRMED;
        }
        return null;
    }
}
