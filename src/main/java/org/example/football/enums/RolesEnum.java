package org.example.football.enums;

public enum RolesEnum {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_USER_CONFIRMED
}
