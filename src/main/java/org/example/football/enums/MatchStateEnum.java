package org.example.football.enums;

public enum MatchStateEnum {
    PLANED,
    STARTED,
    PAUSED,
    DELAYED,
    FINISHED,
    CANCELED,
    ABSENCE
}
