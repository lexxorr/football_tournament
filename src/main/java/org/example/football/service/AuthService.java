package org.example.football.service;

import org.example.football.exception.RegistrationException;
import org.example.football.payload.request.appapi.LoginRequest;
import org.example.football.payload.request.appapi.RegisterNewUserRequest;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    ResponseEntity<?> authUser(LoginRequest loginRequest);
    void registerUser(RegisterNewUserRequest registerNewUserRequest) throws RegistrationException;
    void confirmUserRegistration(String token) throws RegistrationException;
}
