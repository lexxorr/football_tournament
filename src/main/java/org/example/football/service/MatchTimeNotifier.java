package org.example.football.service;

import lombok.RequiredArgsConstructor;
import org.example.football.api.MatterMostApiHandler;
import org.example.football.entity.Match;
import org.example.football.entity.Player;
import org.example.football.enums.MatchStateEnum;
import org.example.football.payload.request.mattermostapi.MattermostMatchStartNotificationRequest;
import org.example.football.repository.MatchRepository;
import org.example.football.repository.PlayersRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MatchTimeNotifier {

    @Value("${matchnotification.delay}")
    private int notificationDelay;

    private final MatchRepository matchRepository;

    private final PlayersRepository playersRepository;

    private final MatterMostApiHandler matterMostApiHandler;

    private List<Long> notifiedGames= new ArrayList();

    public int getDelay(){
        return notificationDelay;
    }

    public void checkMatchBeginTime(){
        ArrayList<Match> plannedMatches = (ArrayList<Match>) matchRepository.findAllByState(MatchStateEnum.PLANED);
        if(!plannedMatches.isEmpty()){
            Calendar nowTime = new GregorianCalendar();
            nowTime.setTime(new Date());

            for(Match match : plannedMatches){
                Long timeDiff = match.getBeginTime().getTime() - (new Date()).getTime();
                if(Math.abs(TimeUnit.MILLISECONDS.toMinutes(timeDiff)) < 10) {
                    List<Player> playersList =  playersRepository.findAllByIdIn(match.getTeamOne());
                    List<Player> opponentList =  playersRepository.findAllByIdIn(match.getTeamTwo());
                    try {
                        notifyTeam(playersList, opponentList);
                        notifyTeam(opponentList, playersList);
                    }
                    catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                }
            }
        }
    }

    private void notifyTeam(List<Player> playersList, List<Player> opponentList){
        String resultOpponents = opponentList.stream()
                .map(p -> p.getNick())
                .collect(Collectors.joining("и", "{", "}"));
        for (Player player : playersList) {
            try {
                MattermostMatchStartNotificationRequest request = new MattermostMatchStartNotificationRequest();
                request.setInviterNick(player.getNick());
                request.setOpponentNick(resultOpponents);
                request.setChannelId(player.getMatterMostChannelId());
                matterMostApiHandler.sendNotificationToUser(request);
            }
            catch (Exception e){
                throw e;
            }
        }
    }
}
