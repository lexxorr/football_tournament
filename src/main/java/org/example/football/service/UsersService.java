package org.example.football.service;

import org.example.football.exception.RegistrationException;
import org.example.football.exception.UnknownUserException;
import org.example.football.payload.request.appapi.DeleteUserRequest;
import org.example.football.payload.request.appapi.RegisterNewUserRequest;

public interface UsersService {
    void deleteUser(DeleteUserRequest deleteUserRequest) throws UnknownUserException, RegistrationException;
    Boolean checkPlayerExist(String nickName);
}
