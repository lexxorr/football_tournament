package org.example.football.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.football.api.ApiHandler;
import org.example.football.entity.PersonalLeaderboard;
import org.example.football.entity.Player;
import org.example.football.enums.RolesEnum;
import org.example.football.exception.RegistrationException;
import org.example.football.model.User;
import org.example.football.payload.request.appapi.LoginRequest;
import org.example.football.payload.request.appapi.RegisterNewUserRequest;
import org.example.football.payload.response.appapi.JwtResponse;
import org.example.football.payload.response.appapi.MessageResponse;
import org.example.football.repository.PlayersRepository;
import org.example.football.repository.RolesRepository;
import org.example.football.repository.UsersRepository;
import org.example.football.security.jwt.JwtUtils;
import org.example.football.service.AuthService;
import org.example.football.service.EmailService;
import org.example.football.service.LeaderboardService;
import org.example.football.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final UsersService usersService;
    private final UsersRepository usersRepository;
    private final PlayersRepository playersRepository;
    private final PasswordEncoder passwordEncoder;
    private final RolesRepository rolesRepository;
    private final LeaderboardService leaderboardService;
    private final ApiHandler matterMostApiHandler;
    private final EmailService emailService;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    @Override
    public ResponseEntity<?> authUser(LoginRequest loginRequest) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getLogin(), loginRequest.getPassword()));
        }
        catch (AuthenticationException ex){
            log.error("loginUser " + ex.getMessage());
            return ResponseEntity.badRequest().body(new MessageResponse("Login or password invalid"));
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getUsername(),
                roles));

    }

    @Override
    public void registerUser(RegisterNewUserRequest registerNewUserRequest) throws RegistrationException {
        if(registerNewUserRequest.getNickName().isEmpty())
            throw new RegistrationException("Nickname can not be empty");
        if(registerNewUserRequest.getPassword().isEmpty())
            throw new RegistrationException("Password can not be empty");
        if(registerNewUserRequest.getEmail().isEmpty())
            throw new RegistrationException("Email can not be empty");
        if(registerNewUserRequest.getName().isEmpty())
            throw new RegistrationException("Your name can not be empty");
        if(registerNewUserRequest.getSurname().isEmpty())
            throw new RegistrationException("Your surname can not be empty");
        if(registerNewUserRequest.getSlackUID().isEmpty())
            throw new RegistrationException("Slack UID can not be empty");
        if(usersService.checkPlayerExist(registerNewUserRequest.getNickName()))
            throw new RegistrationException("User " + registerNewUserRequest.getNickName() + " already exists");

        Player player = new Player();
        User user = new User();

        user.setPassword(passwordEncoder.encode(registerNewUserRequest.getPassword()));
        user.setUserName(registerNewUserRequest.getNickName());
        user.setRoles( Set.of(rolesRepository.findByName(RolesEnum.ROLE_USER)));
        user.setConfirmToken(UUID.randomUUID().toString());

        User savedUser = usersRepository.save(user);

        player.setNick(registerNewUserRequest.getNickName());
        player.setName(registerNewUserRequest.getName());
        player.setSurname(registerNewUserRequest.getSurname());
        player.setUserId(savedUser.getId());
        player.setEmail(registerNewUserRequest.getEmail());
        player.setSlackUID(registerNewUserRequest.getSlackUID());
        Player savedPlayer = playersRepository.save(player);

        PersonalLeaderboard leader = new PersonalLeaderboard();
        leader.setPlayer(savedPlayer);
        leaderboardService.addPlayerToLeaderBoard(leader);

        String appUrl = String.format("%s", matterMostApiHandler.getAppUrl());

        SimpleMailMessage registrationEmail = new SimpleMailMessage();
        registrationEmail.setTo(player.getEmail());
        registrationEmail.setSubject("Registration Confirmation");
        registrationEmail.setText("To confirm your e-mail address, please click the link below:\n"
                + appUrl + "confirm?token=" + user.getConfirmToken());
        registrationEmail.setFrom("yu.stepanov@madbrains.ru");

        emailService.sendEmail(registrationEmail);
    }

    @Override
    public void confirmUserRegistration(String token) throws RegistrationException {
        User user = usersRepository.findUserByConfirmToken(token);
        if(user == null)
            throw new RegistrationException("Unknown user");
        user.setIsEnabled(true);
        usersRepository.save(user);
    }
}
