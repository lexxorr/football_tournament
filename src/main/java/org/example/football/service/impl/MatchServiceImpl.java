package org.example.football.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.football.api.ApiHandler;
import org.example.football.entity.*;
import org.example.football.enums.ApiMessageTypes;
import org.example.football.enums.MatchStateEnum;
import org.example.football.exception.ApiCallArgumentsMissedException;
import org.example.football.exception.MatchCreationException;
import org.example.football.mapper.impl.MatchMapper;
import org.example.football.payload.request.appapi.InviteToMatchRequest;
import org.example.football.payload.request.appapi.MatchResultRequest;
import org.example.football.payload.request.mattermostapi.MattermostInviteRequest;
import org.example.football.repository.LeaderboardRepository;
import org.example.football.repository.MatchRepository;
import org.example.football.repository.MatchRequestRepository;
import org.example.football.repository.PlayersRepository;
import org.example.football.service.MatchService;
import org.example.football.service.components.RatingCalculator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class MatchServiceImpl implements MatchService {
    private final MatchMapper matchMapper;
    private final PlayersRepository playersRepository;
    private final MatchRepository matchRepository;
    private final MatchRequestRepository matchRequestRepository;
    private final ApiHandler matterMostApiHandler;
    private final LeaderboardRepository leaderboardRepository;
    private final RatingCalculator ratingCalculator;

    @Value("${const.matchDelayInMinutes}")
    private Integer matchDelayInMinutes;

    @Override
    public InviteToMatchRequest createMatch(InviteToMatchRequest inviteToMatchRequest) throws MatchCreationException, ApiCallArgumentsMissedException {
        InviteToMatchRequest inviteToMatchRequestRes = new InviteToMatchRequest(inviteToMatchRequest);
        try {
            checkCreateMatchInSpecifiedTime(inviteToMatchRequest.getTime());
            createMatchDraft(inviteToMatchRequestRes);

        } catch (MatchCreationException | ApiCallArgumentsMissedException ex) {
            throw ex;
        }
        return inviteToMatchRequestRes;
    }

    private void createMatchDraft(InviteToMatchRequest inviteToMatchRequestRes) throws ApiCallArgumentsMissedException {
        Boolean needSaveChannel;
        MatchRequest matchRequest = new MatchRequest();
        matchRequest.setAcceptors(inviteToMatchRequestRes.getAcceptorsList());
        matchRequest.setInviters(inviteToMatchRequestRes.getInviters());
        matchRequest.setUuid(UUID.randomUUID().toString());
        matchRequest.setBeginTime(inviteToMatchRequestRes.getTime());
        MatchRequest request = matchRequestRepository.save(matchRequest);

        List<Player> inviterPlayers = playersRepository.findAllByIdIn(inviteToMatchRequestRes.getInviters());
        List<Player> acceptorPlayers = playersRepository.findAllByIdIn(inviteToMatchRequestRes.getAcceptorsList());

        inviteToMatchRequestRes.setRequestId(request.getId());
        inviteToMatchRequestRes.setInviteState(InviteStateEnum.INVITE_DRAFT);
        HashMap<String, String> apiParams = new HashMap<>();
        MattermostInviteRequest inviteRequest = new MattermostInviteRequest();
        inviteRequest.setApiUrl(matterMostApiHandler.getAppUrl());
        inviteRequest.setMatchRequestUUID(request.getUuid());
        inviteRequest.setTime(request.getBeginTime().toString());
        inviteRequest.setTeamOne(inviterPlayers.stream().map(p -> p.getNick()).collect(Collectors.toList()));
        inviteRequest.setTeamTwo(acceptorPlayers.stream().map(p -> p.getNick()).collect(Collectors.toList()));
        for (Player player : acceptorPlayers) {
            needSaveChannel = fillInviteCallApiParams(player, apiParams);
            inviteRequest.setIsFirstTeamCall(true);
            inviteRequest.setPlayerNick(player.getNick());
            matterMostApiHandler.sendMessage(ApiMessageTypes.INVITE_PLAYER_MESSAGE, apiParams, inviteRequest);
            saveUUID(player, apiParams.get("userId"), needSaveChannel);
            apiParams.clear();
        }
        if (inviterPlayers.size() > 1) {
            needSaveChannel = fillInviteCallApiParams(inviterPlayers.get(1), apiParams);
            inviteRequest.setTeamOne(List.of(inviterPlayers.get(1).getNick()));
            inviteRequest.setPlayerNick(inviterPlayers.get(1).getNick());
            inviteRequest.setIsFirstTeamCall(false);
            matterMostApiHandler.sendMessage(ApiMessageTypes.INVITE_PLAYER_MESSAGE, apiParams, inviteRequest);
            saveUUID(inviterPlayers.get(1), apiParams.get("userId"), needSaveChannel);
        }
    }

    @Transactional
    public void createMatchSuccess(String requestUUID) {
        Match match = new Match();
        MatchRequest matchRequest = matchRequestRepository.findByUuid(requestUUID);
        match.setBeginTime(new Date());

        List<Long> inviters = new ArrayList<>();
        inviters.addAll(matchRequest.getInviters());

        List<Long> acceptors = new ArrayList<>();
        acceptors.addAll(matchRequest.getAcceptors());

        Integer requiredAcceptors = inviters.size() + acceptors.size() - 1;

        match.setTeamOne(inviters);
        match.setTeamTwo(acceptors);
        match.setBeginTime(matchRequest.getBeginTime());
        match.setState(MatchStateEnum.PLANED);
        matchRepository.save(match);
        if ( (matchRequest.getAcceptedCount() - 1) == requiredAcceptors ) {
            createMatchReject(requestUUID);
        }
        else {
            matchRequest.setAcceptedCount(matchRequest.getAcceptedCount() + 1);
            matchRequestRepository.save(matchRequest);
        }
    }

    @Transactional
    public void createMatchReject(String requestUUID) {
        matchRequestRepository.deleteByUuid(requestUUID);
    }

    @Transactional
    public void storeMatch(MatchResultRequest matchResultRequest) {
        Boolean firstTeamWon = false;
        matchResultRequest.setBeginTime(new Date());
        matchResultRequest.setState(MatchStateEnum.PLANED);
        matchRepository.save(matchMapper.toEntity(matchResultRequest));

        PersonalLeaderboard playerOneLeaderboardRec = leaderboardRepository.findAllByPlayerId(matchResultRequest.getPlayerOne()).get(0);
        PersonalLeaderboard playerTwoLeaderboardRec = leaderboardRepository.findAllByPlayerId(matchResultRequest.getPlayerTwo()).get(0);

        System.out.println(playerOneLeaderboardRec);
        playerOneLeaderboardRec.setGoalsScored(playerOneLeaderboardRec.getGoalsScored() + matchResultRequest.getPlayerOneScore());
        playerOneLeaderboardRec.setGoalsPassed(playerOneLeaderboardRec.getGoalsPassed() + matchResultRequest.getPlayerTwoScore());

        playerTwoLeaderboardRec.setGoalsScored(playerTwoLeaderboardRec.getGoalsScored() + matchResultRequest.getPlayerTwoScore());
        playerTwoLeaderboardRec.setGoalsPassed(playerTwoLeaderboardRec.getGoalsPassed() + matchResultRequest.getPlayerOneScore());

        if (matchResultRequest.getPlayerOneScore() > matchResultRequest.getPlayerTwoScore()) {
            playerOneLeaderboardRec.setWins(playerOneLeaderboardRec.getWins() + 1);
            playerTwoLeaderboardRec.setLoses(playerTwoLeaderboardRec.getLoses() + 1);
        } else {
            playerTwoLeaderboardRec.setWins(playerTwoLeaderboardRec.getWins() + 1);
            playerOneLeaderboardRec.setLoses(playerOneLeaderboardRec.getLoses() + 1);
        }
        leaderboardRepository.save(playerOneLeaderboardRec);
        leaderboardRepository.save(playerTwoLeaderboardRec);
    }

    private Boolean checkCreateMatchInSpecifiedTime(Date time) throws MatchCreationException {
        if (new Date().getTime() > time.getTime())
            throw new MatchCreationException("Can`t create match in past");
        Calendar c = new GregorianCalendar();
        c.setTime(time);
        c.add(Calendar.MINUTE, -matchDelayInMinutes);
        Date intervalBegin = c.getTime();
        c.add(Calendar.MINUTE, 2 * matchDelayInMinutes);
        Date intervalEnd = c.getTime();
        ArrayList<Match> matchListByDate = (ArrayList<Match>) matchRepository.findAllByBeginTimeBetweenAndState(intervalBegin,
                intervalEnd, MatchStateEnum.PLANED);
        if (!matchListByDate.isEmpty())
            throw new MatchCreationException("Can`t create match : some games already planed in this time");
        return true;
    }

    private void saveUUID(Player player, String UUID, Boolean needSave){
        if (needSave) {
            player.setMatterMostChannelId(UUID);
            playersRepository.save(player);
        }
    }

    private Boolean fillInviteCallApiParams(Player player, HashMap<String, String> apiParams) {
        Boolean needSaveUUID = false;
        apiParams.put("email", "");
        apiParams.put("userId", "");
        if (player.getMatterMostChannelId() == null) {
            apiParams.put("email", player.getEmail());
            needSaveUUID = true;
        }
        else {
            apiParams.put("userId", player.getMatterMostChannelId());
        }
        return needSaveUUID;
    }
}
