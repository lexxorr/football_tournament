package org.example.football.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.football.entity.PersonalLeaderboard;
import org.example.football.payload.response.appapi.LeaderboardResponse;
import org.example.football.repository.LeaderboardRepository;
import org.example.football.service.LeaderboardService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class LeaderboardServiceImpl implements LeaderboardService {

    private final LeaderboardRepository leaderboardRepository;

    @Override
    public void addPlayerToLeaderBoard(PersonalLeaderboard leaderboard) {
        leaderboardRepository.save(leaderboard);

    }

    @Override
    public ArrayList<LeaderboardResponse> getLeaderboard() {
        ArrayList<LeaderboardResponse> responses = new ArrayList<LeaderboardResponse>();
        Integer rankCounter = 1;
        for (PersonalLeaderboard leaderboardRec : leaderboardRepository.getAllSorted()) {
            LeaderboardResponse leaderboardResponse = new LeaderboardResponse();
            leaderboardResponse.setRank(rankCounter);
            leaderboardResponse.setNick(leaderboardRec.getPlayer().getNick());
            leaderboardResponse.setLoses(leaderboardRec.getLoses());
            leaderboardResponse.setWins(leaderboardRec.getLoses());
            leaderboardResponse.setGoalsDifference(String.format("%d - %d", leaderboardRec.getGoalsScored(),
                    leaderboardRec.getGoalsPassed()));
            leaderboardResponse.setScores(leaderboardRec.getScore());
            responses.add(leaderboardResponse);
            ++rankCounter;
        }
        return responses;
    }
}