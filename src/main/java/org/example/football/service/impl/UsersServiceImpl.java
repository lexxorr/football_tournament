package org.example.football.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.football.exception.UnknownUserException;
import org.example.football.payload.request.appapi.DeleteUserRequest;
import org.example.football.repository.UsersRepository;
import org.example.football.service.UsersService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;

    @Override
    @Transactional
    public void deleteUser(DeleteUserRequest deleteUserRequest) throws UnknownUserException {
        if(!checkPlayerExist(deleteUserRequest.getUserName()))
            throw new UnknownUserException(deleteUserRequest.getUserName());
        usersRepository.deleteByUserName(deleteUserRequest.getUserName());
    }

    @Override
    public Boolean checkPlayerExist(String nickName){
        return usersRepository.findByUserName(nickName) == null ? false : true;
    }
}
