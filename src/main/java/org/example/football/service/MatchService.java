package org.example.football.service;


import org.example.football.exception.ApiCallArgumentsMissedException;
import org.example.football.exception.MatchCreationException;
import org.example.football.payload.request.appapi.InviteToMatchRequest;

public interface MatchService {
    InviteToMatchRequest createMatch(InviteToMatchRequest inviteToMatchRequest) throws MatchCreationException, ApiCallArgumentsMissedException;
}
