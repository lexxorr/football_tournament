package org.example.football.service;

import org.example.football.entity.PersonalLeaderboard;
import org.example.football.payload.response.appapi.LeaderboardResponse;

import java.util.ArrayList;

public interface LeaderboardService {
    void addPlayerToLeaderBoard(PersonalLeaderboard leaderboard);
    ArrayList<LeaderboardResponse> getLeaderboard();
}
