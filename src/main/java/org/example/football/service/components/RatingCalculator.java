package org.example.football.service.components;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.TreeMap;

@Component
public class RatingCalculator {

    private Integer maxDelta = 24;

    public void calcRating(TreeMap<Long, Integer>  teamOneRating, TreeMap<Long, Integer>  teamTwoRating,
                                   Boolean isTeamOneWon){
        Integer teamOneMMR = (int) teamOneRating.values().stream().mapToInt(value -> value).average().getAsDouble();
        Integer teamTwoMMR = (int) teamTwoRating.values().stream().mapToInt(value -> value).average().getAsDouble();
        Integer multiplier = teamOneRating.size() == teamTwoRating.size() ? 1 : 2;
        recalcTeamRating(teamOneRating, teamOneMMR, teamTwoMMR, multiplier, isTeamOneWon);
        recalcTeamRating(teamTwoRating, teamTwoMMR, teamOneMMR, multiplier, !isTeamOneWon);
    }

    private Double calcScoresIncome(Integer ownRating, Integer otherRating){
        return 1 / (1 + Math.pow(10, ((otherRating - ownRating) / 400)));
    }

    private void recalcTeamRating(TreeMap<Long, Integer>  teamRating, Integer teamOneMMR, Integer teamTwoMMR,
                                  Integer ratingMultiplier, Boolean isTeamWon){
        for (Map.Entry<Long, Integer> entry : teamRating.entrySet()) {
            Long key = entry.getKey();
            Integer rating = entry.getValue();
            Integer newRating = calcNewRating(rating, teamOneMMR, teamTwoMMR, ratingMultiplier, isTeamWon);
            teamRating.replace(key, newRating);
        }
    }

    private Integer calcNewRating(Integer startRating, Integer teamOneMMR, Integer teamTwoMMR, Integer ratingMultiplier,
                                  Boolean isTeamWon){
        Integer winPoints = isTeamWon ? 1 : 0;
        Double newRatingIncome = calcScoresIncome(startRating, teamTwoMMR);
        Integer newRatingDelta = ((int) (ratingMultiplier * maxDelta * (winPoints - newRatingIncome)));
        if (startRating < 100 && newRatingDelta < 0)
            return startRating;
        else if (startRating < 100 && newRatingDelta > 0)
            return startRating + maxDelta;
        else
            return startRating + newRatingDelta;
    }
}
