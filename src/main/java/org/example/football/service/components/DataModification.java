package org.example.football.service.components;

@FunctionalInterface
public interface DataModification {
    void go(String dataKey);
}
