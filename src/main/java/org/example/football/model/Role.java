package org.example.football.model;

import lombok.Data;
import org.example.football.enums.RolesEnum;
import org.example.football.enums.converters.MatchStateEnumConverter;
import org.example.football.enums.converters.RolesEnumConverter;

import javax.persistence.*;

@Entity
@Data
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private RolesEnum name;
}