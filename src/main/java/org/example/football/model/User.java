package org.example.football.model;

import lombok.Data;
import lombok.ToString;
import org.example.football.entity.Player;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Data
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login")
    private String userName;

    private String password;

    @Column(name = "is_enabled")
    private Boolean isEnabled = false;

    @Column(name = "confirm_token")
    private String confirmToken;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(
            name="user_role",
            joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns=@JoinColumn(name="role_id", referencedColumnName="id")
    )
    private Set<Role> roles;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "players",
            joinColumns = @JoinColumn(name="user_id", referencedColumnName="id")
    )
    @ToString.Exclude
    private Player players;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<SimpleGrantedAuthority> authList = new TreeSet();

        for (Role role : roles) {
            authList.add(new SimpleGrantedAuthority(role.getName().toString()));
        }
        return authList;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public String getPassword(){
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }
}
