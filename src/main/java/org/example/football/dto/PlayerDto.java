package org.example.football.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PlayerDto  {
    private String nickName;
    private String name;
    private String surname;
    private String password;
}
