package org.example.football.controller;

import lombok.RequiredArgsConstructor;
import org.example.football.service.impl.LeaderboardServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LeaderboardController {

    private final LeaderboardServiceImpl leaderboardService;
    @GetMapping("/leaderboard")
    ResponseEntity<?> getLeaderboard(){
        return ResponseEntity.ok(leaderboardService.getLeaderboard());

    }
}
