package org.example.football.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.football.payload.request.appapi.LoginRequest;
import org.example.football.payload.response.appapi.JwtResponse;
import org.example.football.payload.response.appapi.MessageResponse;
import org.example.football.security.jwt.JwtUtils;
import org.example.football.service.AuthService;
import org.example.football.service.impl.UsersServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AuthorisationController {
    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginRequest loginRequest){
        log.info("loginUser " + loginRequest.toString());

        return authService.authUser(loginRequest);
    }
}
