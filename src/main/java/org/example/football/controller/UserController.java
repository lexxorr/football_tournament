package org.example.football.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.example.football.exception.ApiCallArgumentsMissedException;
import org.example.football.exception.MatchCreationException;
import org.example.football.payload.request.appapi.InviteToMatchRequest;
import org.example.football.payload.request.appapi.MatchResultRequest;
import org.example.football.payload.response.appapi.MessageResponse;
import org.example.football.service.impl.MatchServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("user")
@Log4j2
public class UserController {

    private final MatchServiceImpl matchService;

    @PostMapping("/create_match")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<?> createMatch(
            @RequestBody InviteToMatchRequest inviteToMatchRequest
    ){
        log.info("createMatch! Received request " + inviteToMatchRequest.toString());
        try {
            return ResponseEntity.ok(
//                SlackRequestResponse.generate("Vasya",
//                        "U02GKRKDFRB",
//                        new Date().toString(), "3d204cb4-5e14-4056-9394-c7799d473aab")
                    matchService.createMatch(inviteToMatchRequest)
            );
        }
        catch (MatchCreationException | ApiCallArgumentsMissedException ex){
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @PostMapping("/create_match_success")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<?> createMatchSuccess(
            @RequestBody String inviteToMatchRequest
    ){
        return ResponseEntity.ok(""
//                SlackRequestResponse.generate("Vasya",
//                        "U02GKRKDFRB",
//                        new Date().toString(), "3d204cb4-5e14-4056-9394-c7799d473aab")

        );
    }

    @PostMapping("/add_result")
    public ResponseEntity<?> addMatchResult(@RequestBody MatchResultRequest matchResultRequest) {
        matchService.storeMatch(matchResultRequest);
        return ResponseEntity.ok(new MessageResponse("Match result stored"));
    }

    @PostMapping("/create_match_accept/{uuid}")
    public ResponseEntity<?> addMatchSuccess(@PathVariable(name = "uuid") String uuid) {
        matchService.createMatchSuccess(uuid);
        return ResponseEntity.ok(new MessageResponse("Match result accepted"));
    }

    @PostMapping("/create_match_reject/{uuid}")
    public ResponseEntity<?> addMatchReject(@PathVariable(name = "uuid") String uuid) {
        matchService.createMatchReject(uuid);
        return ResponseEntity.ok(new MessageResponse("Match result rejected"));
    }
}
