package org.example.football.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.football.exception.UnknownUserException;
import org.example.football.payload.request.appapi.DeleteUserRequest;
import org.example.football.payload.response.appapi.MessageResponse;
import org.example.football.service.impl.UsersServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("admin")
@RequiredArgsConstructor
@Slf4j
public class AdminController {
    private final UsersServiceImpl usersService;

    @PostMapping("/delete_user")
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    ResponseEntity<?> deleteUser(@RequestBody DeleteUserRequest deleteUserRequest){
        log.info("deleteUser " + deleteUserRequest.toString());
        try {
            usersService.deleteUser(deleteUserRequest);
        }
        catch (UnknownUserException e) {
            log.error("deleteUser " + e.getMessage());
            ResponseEntity.badRequest().body(new MessageResponse(e.getMessage()));
        }
        return ResponseEntity.ok(new MessageResponse("User " + deleteUserRequest.getUserName() + " has been deleted"));
    }

}
