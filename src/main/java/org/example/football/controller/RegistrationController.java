package org.example.football.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.football.exception.RegistrationException;
import org.example.football.payload.request.appapi.RegisterNewUserRequest;
import org.example.football.payload.response.appapi.MessageResponse;
import org.example.football.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RegistrationController {
    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<?> userRegistration(@RequestBody RegisterNewUserRequest registerNewUserRequest)  {
        try {
            authService.registerUser(registerNewUserRequest);
        }
        catch (RegistrationException registrationException) {
            log.error("userRegistration " + registrationException.getMessage());
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(registrationException.getMessage()));
        }
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @GetMapping("/confirm")
    public ResponseEntity<?> userConfirmation(@RequestParam("token") String token){
        try {
            authService.confirmUserRegistration(token);
        } catch (RegistrationException e) {
            log.error("userRegistration " + e.getMessage());
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(e.getMessage()));

        }
        return ResponseEntity.ok(new MessageResponse("User confirmed successfully!"));
    }
}
