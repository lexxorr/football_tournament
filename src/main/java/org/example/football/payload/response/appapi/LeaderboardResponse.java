package org.example.football.payload.response.appapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
public class LeaderboardResponse {

    @JsonIgnoreProperties("rank")
    private Integer rank;

    @JsonIgnoreProperties("nick")
    private String nick;

    @JsonIgnoreProperties("wins")
    private Integer wins;

    @JsonIgnoreProperties("loses")
    private Integer loses;

    @JsonIgnoreProperties("goalsDifference")
    private String goalsDifference;

    @JsonIgnoreProperties("scores")
    private Integer scores;
}
