package org.example.football.payload.response.appapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MessageResponse {
    public MessageResponse(String responseText) {
        message = responseText;
    }
    @JsonProperty("message")
    private String message;
}
