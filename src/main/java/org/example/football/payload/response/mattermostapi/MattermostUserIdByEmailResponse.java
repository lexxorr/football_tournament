package org.example.football.payload.response.mattermostapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MattermostUserIdByEmailResponse {
    @JsonProperty("id")
    private String id;
}
