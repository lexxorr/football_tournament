package org.example.football.payload.response.appapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.example.football.entity.MatchRequsetEntities.Block;
import org.example.football.entity.MatchRequsetEntities.Element;
import org.example.football.entity.MatchRequsetEntities.Field;
import org.example.football.entity.MatchRequsetEntities.Text;

import java.util.ArrayList;

@Data
public class SlackRequestResponse {

    public static SlackRequestResponse generate(String callerName, String opponentUID, String date, String requestId){
        SlackRequestResponse slackRequestResponse = new SlackRequestResponse();
        slackRequestResponse.setChannel(opponentUID);
        slackRequestResponse.setText(requestId);

        Block header = new Block();
        header.setType("header");
        header.setText(new Text("Новый запрос", "plain_text", false));
        slackRequestResponse.addBlock(header);

        Block contentBlock = new Block();
        contentBlock.setType("section");

        Field fromField = new Field();
        fromField.setType("mrkdwn");
        fromField.setText("*От кого:*\n" + callerName);

        Field whenField = new Field();
        whenField.setType("mrkdwn");
        whenField.setText("*Когда:*\n" + date);

        contentBlock.addField(fromField);
        contentBlock.addField(whenField);
        slackRequestResponse.addBlock(contentBlock);

        Block approveBlock = new Block();
        approveBlock.setType("actions");

        Element acceptElement = new Element();
        acceptElement.setType("button");
        acceptElement.setStyle("primary");
        acceptElement.setValue("click_me_123");
        acceptElement.setText(new Text("Согласиться", "plain_text", true));

        Element rejectElement = new Element();
        rejectElement.setType("button");
        rejectElement.setStyle("danger");
        rejectElement.setValue("click_me_123");
        rejectElement.setText(new Text("Отказаться", "plain_text", true));

        approveBlock.addElement(acceptElement);
        approveBlock.addElement(rejectElement);
        slackRequestResponse.addBlock(approveBlock);

        return slackRequestResponse;
    }


    public void addBlock(Block block){
        blocks.add(block);
    }
    @JsonProperty("channel")
    private String channel;

    @JsonProperty("text")
    private String text;

    @JsonProperty("blocks")
    private ArrayList<Block> blocks = new ArrayList<>();
}
