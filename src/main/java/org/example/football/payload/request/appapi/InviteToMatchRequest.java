package org.example.football.payload.request.appapi;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.example.football.entity.InviteStateEnum;

import javax.persistence.ElementCollection;
import java.util.Date;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class InviteToMatchRequest {

    public InviteToMatchRequest(){};

    public InviteToMatchRequest(InviteToMatchRequest other) {
        this.requestId = other.requestId;
        this.acceptorsList = other.acceptorsList;
        this.inviters = other.inviters;
        this.inviteState = other.inviteState;
        this.time = other.time;
    }

    @JsonProperty("requestId")
    Long requestId;

    @JsonProperty("beginTime")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone="GMT+04:00")
    private Date time;

    @JsonProperty("inviters")
    private List<Long> inviters;

    @JsonProperty("acceptors")
    private List<Long> acceptorsList;

    @JsonProperty("inviteState")
    private InviteStateEnum inviteState;
}
