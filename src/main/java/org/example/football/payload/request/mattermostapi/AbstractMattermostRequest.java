package org.example.football.payload.request.mattermostapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.example.football.entity.mattermostmessage.Props;
import org.springframework.beans.factory.annotation.Value;

@Data
public abstract class AbstractMattermostRequest {

    @JsonProperty("channel_id")
    private String channelId;

    @JsonProperty("message")
    private String message;

    @JsonProperty("props")
    private Props props;

    public abstract void generate();
}
