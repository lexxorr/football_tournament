package org.example.football.payload.request.appapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest {
    @JsonProperty("login")
    private String login;

    @JsonProperty("password")
    private String password;
}
