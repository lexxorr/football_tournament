package org.example.football.payload.request.appapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DeleteUserRequest {

    @JsonProperty("userName")
    private String userName;
}
