package org.example.football.payload.request.mattermostapi;

import lombok.Data;
import org.example.football.entity.mattermostmessage.*;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class MattermostInviteRequest extends AbstractMattermostRequest {

    private String opponentChannelId;

    private List<String> teamOne;

    private List<String> teamTwo;

    private String playerNick;

    private Boolean isFirstTeamCall;

    private String time;

    private String apiUrl;

    private String matchRequestUUID;

    @Override
    public void generate() {
        setChannelId(opponentChannelId);
        setMessage(String.format("Уважвемый %s!", playerNick));

        ActionContext cancelContext = new ActionContext();
        cancelContext.setAction("do_something_cancel");

        ActionIntegration cancelActionIntegration = new ActionIntegration();
        cancelActionIntegration.setUrl(apiUrl + "user/create_match_reject/" + matchRequestUUID);
        cancelActionIntegration.setContext(cancelContext);

        Action cancelAction = new Action();
        cancelAction.setId("cancel");
        cancelAction.setName("Отказаться");
        cancelAction.setStyle("danger");
        cancelAction.setIntegration(cancelActionIntegration);

        ActionContext acceptContext = new ActionContext();
        acceptContext.setAction("do_something_accept");

        ActionIntegration acceptActionIntegration = new ActionIntegration();
        acceptActionIntegration.setUrl(apiUrl + "user/create_match_accept/" + matchRequestUUID);
        acceptActionIntegration.setContext(cancelContext);

        Action acceptAction = new Action();
        acceptAction.setId("accept");
        acceptAction.setName("Согласиться");
        acceptAction.setStyle("success");
        acceptAction.setIntegration(acceptActionIntegration);

        Attachment attachment = new Attachment();
        attachment.setPretext("");
        String callers;
        String callVerb;
        String opponents = " ";
        opponents += teamTwo.stream().filter(p -> !p.equals(playerNick)).collect(Collectors.joining(","));

        if (teamOne.size() > 1) {
            callers = "Игроки " + teamOne.stream().collect(Collectors.joining(" и "));
            callVerb = "приглашают";
            if (isFirstTeamCall)
                opponents = "и " + opponents;
        } else {
            callers = "Игрок " + teamOne.stream().collect(Collectors.joining(" и "));
            callVerb = "приглашает";
        }
        if (isFirstTeamCall) {
            attachment.setText(String.format("%s %s вас %s сыграть в %s.", callers, callVerb, opponents, time));
        }
        else{
            attachment.setText(String.format("%s %s вас сыграть против %s в %s.", callers, callVerb, opponents, time));
        }

        attachment.setActions(new Action[]{acceptAction, cancelAction});

        Props props = new Props();
        props.setAttachments(new Attachment[]{attachment});

        setProps(props);
        System.out.println(props);
    }
}
