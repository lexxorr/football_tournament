package org.example.football.payload.request.mattermostapi;

import lombok.Data;

@Data
public class MattermostMatchStartNotificationRequest extends AbstractMattermostRequest{

    private String opponentChannelId;

    private String opponentNick;

    private String inviterNick;

    @Override
    public void generate() {
        setChannelId(opponentChannelId);
        setMessage(String.format("Уважаемый %s,до игры с %s осталось чуть менее десяти минут", opponentNick, inviterNick));
    }
}
