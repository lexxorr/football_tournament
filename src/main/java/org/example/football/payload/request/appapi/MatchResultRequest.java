package org.example.football.payload.request.appapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.example.football.enums.MatchStateEnum;
import org.example.football.enums.converters.MatchStateEnumConverter;
import org.example.football.mapper.AbstractDto;
;
import javax.persistence.Convert;
import java.util.Date;

@Data
public class MatchResultRequest extends AbstractDto {

        @JsonProperty("beginTime")
        private Date beginTime;

        @JsonProperty("playerOne")
        private Long playerOne;

        @JsonProperty("playerTwo")
        private Long playerTwo;

        @JsonProperty("playerOneScore")
        private Integer playerOneScore;

        @JsonProperty("playerTwoScore")
        private Integer playerTwoScore;

        @JsonProperty("state")
        @Convert(converter = MatchStateEnumConverter.class)
        private MatchStateEnum state;
}
