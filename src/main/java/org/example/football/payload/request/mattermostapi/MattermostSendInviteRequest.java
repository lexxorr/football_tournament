package org.example.football.payload.request.mattermostapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.example.football.entity.mattermostmessage.*;
import org.springframework.beans.factory.annotation.Value;
import lombok.Data;
import org.springframework.security.core.parameters.P;

@Data
public class MattermostSendInviteRequest {

    @JsonProperty("channel_id")
    private String channelId;

    @JsonProperty("message")
    private String message;

    @JsonProperty("props")
    private Props props;

    public static MattermostSendInviteRequest generateInviteRequest(String opponentChannelId, String opponentNick, String inviterNick,
                                                String time, String apiUrl, String matchRequestUUID){
        MattermostSendInviteRequest mattermostSendInviteRequest = new MattermostSendInviteRequest();
        mattermostSendInviteRequest.setChannelId(opponentChannelId);
        mattermostSendInviteRequest.setMessage(String.format("Уважвемый %s!", opponentNick));

        ActionContext cancelContext = new ActionContext();
        cancelContext.setAction("do_something_cancel");

        ActionIntegration cancelActionIntegration = new ActionIntegration();
        cancelActionIntegration.setUrl(apiUrl + "user/create_match_reject/"+matchRequestUUID);
        cancelActionIntegration.setContext(cancelContext);

        Action cancelAction = new Action();
        cancelAction.setId("cancel");
        cancelAction.setName("Отказаться");
        cancelAction.setStyle("danger");

        ActionContext acceptContext = new ActionContext();
        acceptContext.setAction("do_something_accept");

        ActionIntegration acceptActionIntegration = new ActionIntegration();
        acceptActionIntegration.setUrl(apiUrl + "user/create_match_accept/"+matchRequestUUID);
        acceptActionIntegration.setContext(cancelContext);

        Action acceptAction = new Action();
        acceptAction.setId("accept");
        acceptAction.setName("Согласиться");
        acceptAction.setStyle("success");

        Attachment attachment = new Attachment();
        attachment.setPretext("");
        attachment.setText(String.format("Игрок %s приглашает вас сыграть в %s.",inviterNick, time));
        attachment.setActions(new Action[]{acceptAction, cancelAction} );

        Props props = new Props();
        props.setAttachments(new Attachment[] {attachment} );

        mattermostSendInviteRequest.setProps(props);

        return mattermostSendInviteRequest;

    }

    public static MattermostSendInviteRequest generateMatchStartNotification(String opponentChannelId, String opponentNick, String inviterNick){
        MattermostSendInviteRequest mattermostSendInviteRequest = new MattermostSendInviteRequest();
        mattermostSendInviteRequest.setChannelId(opponentChannelId);
        mattermostSendInviteRequest.setMessage(String.format("Уважаемый %s,до игры с %s осталось чуть менее десяти минут",
                opponentNick, inviterNick));

        return mattermostSendInviteRequest;
    }
}
