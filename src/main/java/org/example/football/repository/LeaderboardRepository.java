package org.example.football.repository;

import org.example.football.entity.PersonalLeaderboard;
import org.hibernate.annotations.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeaderboardRepository extends JpaRepository<PersonalLeaderboard, Long> {
    @Query(value = "select * from leaderboard join players " +
            "ON players.id = player_id " +
            "ORDER BY score,  goals_scored - goals_passed, nick", nativeQuery = true)
    List<PersonalLeaderboard> getAllSorted();

    List<PersonalLeaderboard> findAllByPlayerId(Long id);
}
