package org.example.football.repository;

import org.example.football.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface PlayersRepository extends JpaRepository<Player, Long> {

    Player findByNick(String nick);
    List<Player> findAllByIdIn(List<Long> ids);
}
