package org.example.football.repository;

import org.example.football.model.Role;
import org.example.football.enums.RolesEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {
    Role findByName(RolesEnum name);
}
