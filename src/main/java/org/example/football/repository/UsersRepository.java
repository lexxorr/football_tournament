package org.example.football.repository;

import org.example.football.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    User findByUserName (String login);
    void deleteByUserName(String login);
    User findUserByConfirmToken(String confirmToken);
}
