package org.example.football.repository;

import org.example.football.entity.MatchRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchRequestRepository extends JpaRepository<MatchRequest, Long> {
    MatchRequest findByUuid(String Uuid);

    void deleteByUuid(String Uuid);

}
