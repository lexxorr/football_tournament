package org.example.football.repository;

import org.example.football.entity.Match;
import org.example.football.enums.MatchStateEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Convert;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {

    @Convert(converter = MatchStateEnum.class)
    List<Match> findAllByState(MatchStateEnum matchStateEnum);

    @Convert(converter = MatchStateEnum.class)
    List<Match> findAllByBeginTimeBetweenAndState(Date intervalBegin, Date intervalEnd, MatchStateEnum matchStateEnum);
}
