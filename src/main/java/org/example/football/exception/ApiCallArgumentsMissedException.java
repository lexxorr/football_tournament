package org.example.football.exception;

import org.example.football.enums.ApiMessageTypes;

public class ApiCallArgumentsMissedException extends Exception{
    public ApiCallArgumentsMissedException(String argumentName) {
        super(String.format("Missed parameter \"%s\" in api call", argumentName));
    }
}
