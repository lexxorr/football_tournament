package org.example.football.exception;

public class UnknownUserException extends Exception{
    public UnknownUserException(String userName) { this.userName = userName; }

    @Override
    public String getMessage() {
        return "Unknown user " + userName;
    }

    private String userName;
}
