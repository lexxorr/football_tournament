package org.example.football.exception;

public class RegistrationException extends Exception{
    public RegistrationException(String message) {
        super(message);
    }
}
