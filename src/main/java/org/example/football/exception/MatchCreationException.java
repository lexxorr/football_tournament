package org.example.football.exception;

public class MatchCreationException extends Exception{
    public MatchCreationException(String msg) {
        super(msg);
    }
}
